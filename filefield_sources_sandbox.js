(function($) {

Drupal.sandbox = Drupal.sandbox || {};


/**
 * Attaches the Jcupload behavior to each FileField Sources Jcupload form element.
 */
Drupal.behaviors.filefield_sources_sandbox = {
  attach: function (context, settings) {
    $("input[id$='sandbox-upload-button']", context).once('ffs-sandbox-init', function () {
        var iid = this.id+'_img';
        var cid = this.id+'_cvs';
	this.addEventListener("click", function (event) {
	    var maxSize = settings.sandbox.max_filesize;
	    var input = $("input[id$=jud]")[0];
	    var file = input.files[0];
	    var n = document.createElement('DIV');
	    n.style.display = 'none';
	    n.innerHTML = "<img id='"+iid+"'/><canvas id='"+cid+"'></canvas>";
	    this.parentNode.appendChild(n);
	    reader = new FileReader();
            reader.onload = function(event) {
	       var i = $('#'+iid)[0];
	       i.onload = function(e) {
		var lowQ = 0;
		var highQ = 1;
		var quality = 0.90;

		var cvs = $('#'+cid)[0];
		var ctx = cvs.getContext("2d");

		ctx.drawImage(i, 0, 0, i.naturalWidth, i.naturalHeight);

		var cb = function(b) {

			if(b.size > maxSize) {
				highQ = quality;
				quality = lowQ + (highQ - lowQ) / 2.0;
				if(Math.abs(highQ-lowQ)>0.05) {
					cvs.toBlob(cb, "image/jpeg", quality);
					return;
				}
			}
			else if(b.size < (maxSize*0.85)) {
				lowQ = quality;
				quality = lowQ + (highQ - lowQ) / 2.0;
				if(Math.abs(highQ-lowQ)>0.05) {
					cvs.toBlob(cb, "image/jpeg", quality);
					return;
				}
			}
			alert(file.name+" compressed to "+b.size);
		}

		cvs.toBlob(cb, "image/jpeg", quality);
	       }
               i.src = event.target.result;
	    }
            reader.readAsDataURL(file);
	    event.preventDefault();
	    return false;
        }, false);
    });
  }
}

})(jQuery);
